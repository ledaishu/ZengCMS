layui.define(['layer', 'form', 'table', 'notice', 'clipboard' ,'element','dragsort'], function(exports) {
    var MOD_NAME = 'zengForm',
    $ = layui.$,//$
    layer = layui.layer,//弹框
    form = layui.form,//表单
    table = layui.table,//数据表格
    notice = layui.notice,//注意
    clipboard =  layui.clipboard,//点击复制
    element = layui.element,//常用元素操作
    dragsort = layui.dragsort;//拖动
    // 文件上传集合
    var webuploaders = [];
    // 当前上传对象
    var curr_uploader = {};
    var obj = {
        method1:function(a,b){
            alert('调用方法[method1],参数a='+a+' b='+b);
        },
        method2:function(a,b){
            alert('调用方法[method2],参数a='+a+' b='+b);
        }
    }
    // 放大图片
    $('body').on('click', '[data-image]', function() {
        var title = $(this).attr('data-image'),
            src = $(this).attr('src'),
            alt = $(this).attr('alt');
        var photos = {
            "title": title,
            "id": Math.random(),
            "data": [{
                "alt": alt,
                "pid": Math.random(),
                "src": src,
                "thumb": src
            }]
        };
        layer.photos({
            photos: photos,
            anim: 5
        });
        return false;
    });
    // 绑定tags标签组件
    if ($(".layui-form .form-tags").size() > 0) {
        layui.define('tagsinput', function(exports) {
            var tagsinput = layui.tagsinput;
            $('.form-tags').each(function() {
                $(this).tagsInput({
                    width: 'auto',
                    defaultText: $(this).data('remark'),
                    height: '26px',
                })
            })
        })
    }
    // 绑定城市选择组件
    if ($("[data-toggle='city-picker']").size() > 0) {
        layui.define('citypicker', function(exports) {
            var citypicker = layui.citypicker;
        });
    }
    // 绑定时间组件
    if ($(".layui-form .datetime").size() > 0) {
    	layui.define('laydate', function(exports) {
    		var laydate = layui.laydate;
	        $(".layui-form .datetime").each(function() {
                // 控件选择类型
                // 类型：String，默认值：date
                // 用于单独提供不同的选择器类型，可选值如下表：
                // type可选值	名称	用途
                // year	年选择器	只提供年列表选择
                // month	年月选择器	只提供年、月选择
                // date	日期选择器	可选择：年、月、日。type默认值，一般可不填
                // time	时间选择器	只提供时、分、秒选择
                // datetime	日期时间选择器	可选择：年、月、日、时、分、秒
                var type = $(this).attr('data-date-type');
	            if (type === undefined || type === '' || type === null) {
	                type = 'datetime';
	            }
	            var options = {
	                elem: this,
                    type: type,
                    trigger: 'click',
                };
                // 自定义格式
                // 类型：String，默认值：yyyy-MM-dd
                // 通过日期时间各自的格式符和长度，来设定一个你所需要的日期格式。layDate 支持的格式如下：
                // 格式符	说明
                // yyyy	年份，至少四位数。如果不足四位，则前面补零
                // y	年份，不限制位数，即不管年份多少位，前面均不补零
                // MM	月份，至少两位数。如果不足两位，则前面补零。
                // M	月份，允许一位数。
                // dd	日期，至少两位数。如果不足两位，则前面补零。
                // d	日期，允许一位数。
                // HH	小时，至少两位数。如果不足两位，则前面补零。
                // H	小时，允许一位数。
                // mm	分钟，至少两位数。如果不足两位，则前面补零。
                // m	分钟，允许一位数。
                // ss	秒数，至少两位数。如果不足两位，则前面补零。
                // s	秒数，允许一位数。
                // 通过上述不同的格式符组合成一段日期时间字符串，可任意排版，如下所示：
                // 格式	示例值
                // yyyy-MM-dd HH:mm:ss	2017-08-18 20:08:08
                // yyyy年MM月dd日 HH时mm分ss秒	2017年08月18日 20时08分08秒
                // yyyyMMdd	20170818
                // dd/MM/yyyy	18/08/2017
                // yyyy年M月	2017年8月
                // M月d日	8月18日
                // 北京时间：HH点mm分	北京时间：20点08分
                // yyyy年的M月某天晚上，大概H点	2017年的8月某天晚上，大概20点
                var format = $(this).attr('data-date-format');
	            if (format !== undefined && format !== '' && format !== null) {
	                options['format'] = format;
                }
                // 开启左右面板范围选择，类型：Boolean/String，默认值：false
                // 如果设置 true，将默认采用 “ - ” 分割。 你也可以直接设置 分割字符。五种选择器类型均支持左右面板的范围选择
                // range: '~' 来自定义分割字符
                var range = $(this).attr('data-date-range');
	            if (range !== undefined) {
                    if(range === 'false'){
                        range = false;
                    }else if(range === 'true'){
                        range = true;
                    }else if(range === null || range === ''){
	                    range = '-';
	                }
	                options['range'] = range;
	            }
	            laydate.render(options);
	        });
    	})
    }
    // 绑定图片选择组件
    if ($('.layui-form .fachoose-image').length > 0) {
    	layui.define('tableSelect', function(exports) {
    		var tableSelect = layui.tableSelect;
	        $.each($('.layui-form .fachoose-image'), function(i, v) {
	            var input_id = $(this).data("input-id") ? $(this).data("input-id") : "",
	                inputObj = $("#" + input_id),
	                multiple = $(this).data("multiple") ? 'checkbox' : 'radio';
	            var $file_list = $('#file_list_' + input_id);
	            tableSelect.render({
	                elem: "#fachoose-" + input_id,
	                searchKey: 'name',
	                searchPlaceholder: '请输入图片名称',
	                table: {
	                    url: GV.image_select_url,
	                    cols: [
	                        [
	                            { type: multiple },
	                            { field: 'id', title: 'ID' },
	                            { field: 'url', minWidth: 120, search: false, title: '图片', imageHeight: 40, align: "center", templet: '<div><img src="{{d.path}}" height="100%"></div>' },
	                            { field: 'name', width: 120, title: '名称' },
	                            { field: 'mime', width: 120, title: 'Mime类型' },
	                            { field: 'create_time', width: 180, title: '上传时间', align: "center", search: 'range' },
	                        ]
	                    ]
	                },
	                done: function(e, data) {
	                    var selectedList = [];
	                    $.each(data.data, function(index, val) {
	                        selectedList[index] = {
	                            file_id: val.id,
	                            file_path: val.path
	                        };
	                    });
	                    selectedList.forEach(function(item) {
	                        var $li = '<div class="file-item thumbnail"><img data-image class="' + input_id + "-" + item.file_id + '" data-original="' + item.file_path + '" src="' + item.file_path + '"><div class="file-panel">';
	                        if (multiple == 'checkbox') {
	                            $li += '<i class="iconfont icon-yidong move-picture"></i> ';
	                        }
	                        $li += '<i class="iconfont icon-tailor cropper" data-input-id="' + item.file_id + '" data-id="' + input_id + '"></i> <i class="iconfont icon-trash remove-picture" data-id="' + item.file_id + '"></i></div></div>';
	                        if (multiple == 'checkbox') {
	                            if (inputObj.val()) {
	                                inputObj.val(inputObj.val() + ',' + item.file_id);
	                            } else {
	                                inputObj.val(item.file_id);
	                            }
	                            $file_list.append($li);
	                        } else {
	                            inputObj.val(item.file_id);
	                            $file_list.html($li);
	                        }
	                    });
	                }
	            })
	        });
    	})
    }
    // 绑定selectpage高级下拉框组件
    if ($(".layui-form .selectpage").size() > 0) {
    	layui.define('selectPage', function(exports) {
    		var selectPage = layui.selectPage;
	        $('.layui-form .selectpage').selectPage({
	            eAjaxSuccess: function(data) {
	                //console.log(data);
	                data.list = typeof data.data !== 'undefined' ? data.data : [];
	                data.totalRow = typeof data.count !== 'undefined' ? data.count : data.data.length;
	                return data;
	            }
	        })
    	})
    }
    // 绑定下拉框单选组件
    if ($('.layui-form .form-selecto').length > 0) {
        layui.define('xmSelect', function(exports) {
            var xmselect = layui.xmSelect;
            $('.layui-form .form-selecto').each(function() {
                var name = $(this).attr("name");
                var data = $(this).data("value");
                xmSelect.render({
                    el: '#' + name,
                    // 开启单选
                    radio: true,
                    // 搜索模式
                    filterable: true,
                    // 输入停止2s后进行搜索过滤
                    delay: 2000,
                    // 分页
                    paging: true,
                    // 每页数量
                    pageSize: 5,
                    // 工具栏
                    toolbar: {show: true},
                    // 自动换行
                    autoRow: true,
                    height: '500px',
                    // 隐藏图标
                    // model: {
                    // 	icon: 'hidden',
                    // },
                    data: data,
                    on: function(data){
                        if(data.arr){
                            var myArray=new Array();
                            for (let i = 0; i < data.arr.length; i++) {
                                myArray.push(data.arr[i].value);
                            }
                            var valuestr = myArray.join(",");
                            $("input[name="+name+"]").val(valuestr);
                        }else{
                            $("input[name="+name+"]").val('');
                        }
                    },
                });
            });
        });
    }
    // 绑定下拉框多选组件
    if ($('.layui-form .form-selects').length > 0) {
        layui.define('xmSelect', function(exports) {
            var xmselect = layui.xmSelect;
            $('.layui-form .form-selects').each(function() {
                var name = $(this).attr("name");
                var data = $(this).data("value");
                xmSelect.render({
                    el: '#' + name,
                    // 搜索模式
                    filterable: true,
                    // 输入停止2s后进行搜索过滤
                    delay: 2000,
                    // 分页
                    paging: true,
                    // 每页数量
                    pageSize: 5,
                    // 工具栏
                    toolbar: {show: true},
                    // 自动换行
                    autoRow: true,
                    height: '500px',
                    // 隐藏图标
                    // model: {
                    // 	icon: 'hidden',
                    // },
                    data: data,
                    on: function(data){
                        if(data.arr){
                            var myArray=new Array();
                            for (let i = 0; i < data.arr.length; i++) {
                                myArray.push(data.arr[i].value);
                            }
                            var valuestr = myArray.join(",");
                            $("input[name="+name+"]").val(valuestr);
                        }else{
                            $("input[name="+name+"]").val('');
                        }
                    },
                });
            });
        });
    }
    // 绑定颜色组件
    if ($('.layui-form .layui-color-box').length > 0) {
        layui.define('colorpicker', function(exports) {
            var colorpicker = layui.colorpicker;
            $('.layui-color-box').each(function() {
                var id = $(this).attr('id');
                colorpicker.render({
                    elem: $(this),
                    color: $("input[name='"+id+"']").val(),
                    done: function(color) {
                        $("input[name='"+id+"']").val(color);
                    }
                });
            });
        })
    }
    // 绑定图片上传组件
    if ($('.js-upload-image,.js-upload-images').length > 0) {
        layui.define('webuploader', function(exports) {
            var webuploader=layui.webuploader;
            $('.js-upload-image,.js-upload-images').each(function() {
                var $input_file = $(this).find('input');
                var $input_file_name = $input_file.attr('id');
                // 图片列表
                var $file_list = $('#file_list_' + $input_file_name);
                // 缩略图参数
                var $thumb = $input_file.data('thumb');
                // 水印参数
                var $watermark = $input_file.data('watermark');
                // 是否多图片上传
                var $multiple = $input_file.data('multiple');
                // 允许上传的后缀
                var $ext = $input_file.data('ext');
                // 图片限制大小
                var $sizelimit = $input_file.data('size')
                var $size = $sizelimit * 1024;
                // 优化retina, 在retina下这个值是2
                var ratio = window.devicePixelRatio || 1;
                // 缩略图大小
                var thumbnailWidth = 100 * ratio;
                var thumbnailHeight = 100 * ratio;
                var uploader = WebUploader.create({
                    // 选完图片后，是否自动上传。
                    auto: true,
                    // 去重
                    duplicate: true,
                    // 不压缩图片
                    resize: false,
                    compress: false,
                    // swf文件路径
                    swf: GV.WebUploader_swf,
                    pick: {
                        id: '#picker_' + $input_file_name,
                        multiple: $multiple
                    },
                    server: GV.image_upload_url,
                    // 图片限制大小
                    fileSingleSizeLimit: $size,
                    // 只允许选择图片文件。
                    accept: {
                        title: 'Images',
                        extensions: $ext,
                        mimeTypes: 'image/jpg,image/jpeg,image/bmp,image/png,image/gif'
                    },
                    // 自定义参数
                    formData: {
                        sizelimit:$sizelimit,
                        extlimit:$ext,
                        thumb: $thumb,
                        watermark: $watermark
                    }
                })
                element.on('tab', function(data){
                    uploader.refresh();
                });
                // console.log(uploader);
                // 当有文件添加进来的时候
                uploader.on('fileQueued', function(file) {
                    var $li = $(
                        '<div id="' + file.id + '" class="file-item js-gallery thumbnail">' +
                        '<img data-image>' +
                        '<div class="info">' + file.name + '</div>' +
                        '<div class="file-panel">' +
                        ($multiple ? ' <i class="iconfont icon-yidong move-picture"></i> ' : '') +
                        '<i class="iconfont icon-tailor cropper"></i> <i class="iconfont icon-trash remove-picture"></i></div><div class="progress progress-mini remove-margin active">' +
                        '<div class="progress-bar progress-bar-primary progress-bar-striped" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" style="width: 0%"></div>' +
                        '</div>' +
                        '<div class="file-state img-state"><div class="layui-bg-blue">正在读取...</div>' +
                        '</div>'
                    ),
                    $img = $li.find('img');
                    if ($multiple) {
                        $file_list.append($li);
                    } else {
                        $file_list.html($li);
                        $input_file.val('');
                    }
                    // 创建缩略图
                    // 如果为非图片文件，可以不用调用此方法。
                    // thumbnailWidth x thumbnailHeight 为 100 x 100
                    uploader.makeThumb(file, function(error, src) {
                        if (error) {
                            $img.replaceWith('<span>不能预览</span>');
                            return;
                        }
                        $img.attr('src', src);
                    }, thumbnailWidth, thumbnailHeight);
                    // 设置当前上传对象
                    curr_uploader = uploader;
                });
                // 文件上传过程中创建进度条实时显示。
                uploader.on('uploadProgress', function(file, percentage) {
                    var $percent = $('#' + file.id).find('.progress-bar');
                    // console.log($percent);
                    $percent.css('width', percentage * 100 + '%');
                });
                // 文件上传成功
                uploader.on('uploadSuccess', function(file, response) {
                    var $li = $('#' + file.id);
                    if (response.code == 0) {
                        if ($multiple) {
                            if ($input_file.val()) {
                                $input_file.val($input_file.val() + ',' + response.id);
                            } else {
                                $input_file.val(response.id);
                            }
                            $li.find('.remove-picture').attr('data-id', response.id);
                        } else {
                            $input_file.val(response.id);
                        }
                    }
                    $li.find('.file-state').html('<div class="layui-bg-green">' + response.info + '</div>');
                    $li.find('img').attr('data-original', response.path).addClass($input_file_name + '-' + response.id);
                    $li.find('.file-panel .cropper').attr('data-input-id', response.id).attr('data-id', $input_file_name);
                    $li.find('.file-panel .remove-picture').attr('data-id', response.id);
                });
                // 文件上传失败，显示上传出错。
                uploader.on('uploadError', function(file) {
                    var $li = $('#' + file.id);
                    $li.find('.file-state').html('<div class="layui-bg-red">服务器错误</div>');
                });
                // 文件验证不通过
                uploader.on('error', function(type) {
                    switch (type) {
                        case 'Q_TYPE_DENIED':
                            layer.alert('图片类型不正确，只允许上传后缀名为：' + $ext + '，请重新上传！', { icon: 5 })
                            break;
                        case 'F_EXCEED_SIZE':
                            layer.alert('图片不得超过' + ($size / 1024) + 'kb，请重新上传！', { icon: 5 })
                            break;
                    }
                });
                // 完成上传完了，成功或者失败，先删除进度条。
                uploader.on('uploadComplete', function(file) {
                    setTimeout(function() {
                        $('#' + file.id).find('.progress').remove();
                    }, 500);
                    if ($multiple) {
                        $file_list.dragsort({
                            //itemSelector:".move-picture",
                            dragSelector: ".move-picture",
                            dragEnd: function() {
                                var ids = [];
                                $file_list.find('.remove-picture').each(function() {
                                    ids.push($(this).data('id'));
                                });
                                $input_file.val(ids.join(','));
                            },
                            placeHolderTemplate: '<div class="file-item thumbnail" style="border:1px #009688 dashed;"></div>'
                        })
                    }
                });
                // 删除图片
                $file_list.delegate('.remove-picture', 'click', function() {
                    $(this).closest('.thumbnail').remove();
                    if ($multiple) {
                        var ids = [];
                        $file_list.find('.remove-picture').each(function() {
                            ids.push($(this).data('id'));
                        });
                        $input_file.val(ids.join(','));
                    } else {
                        $input_file.val('');
                    }
                });
                // 将上传实例存起来
                webuploaders.push(uploader);
                // 如果是多图上传，则实例化拖拽
                if ($multiple) {
                    $file_list.dragsort({
                        // itemSelector:".move-picture",
                        dragSelector: ".move-picture",
                        dragEnd: function() {
                            var ids = [];
                            $file_list.find('.remove-picture').each(function() {
                                ids.push($(this).data('id'));
                            });
                            $input_file.val(ids.join(','));
                        },
                        placeHolderTemplate: '<div class="file-item thumbnail" style="border:1px #009688 dashed;"></div>'
                    })
                }
            });
        })
    }
    // 绑定文件上传组件
    if ($('.js-upload-file,.js-upload-files').length > 0) {
        layui.define('webuploader', function(exports) {
            var webuploader=layui.webuploader;
            $('.js-upload-file,.js-upload-files').each(function() {
                var $input_file = $(this).find('input');
                var $input_file_name = $input_file.attr('id');
                // 是否多文件上传
                var $multiple = $input_file.data('multiple');
                // 允许上传的后缀
                var $ext = $input_file.data('ext');
                // 文件限制大小
                var $sizelimit = $input_file.data('size')
                var $size = $sizelimit * 1024;
                // 文件列表
                var $file_list = $('#file_list_' + $input_file_name);
                // 实例化上传
                var uploader = WebUploader.create({
                    // 选完文件后，是否自动上传。
                    auto: true,
                    // 去重
                    duplicate: true,
                    // swf文件路径
                    swf: GV.WebUploader_swf,
                    // 文件接收服务端。
                    server: GV.file_upload_url,
                    // 选择文件的按钮。可选。
                    // 内部根据当前运行是创建，可能是input元素，也可能是flash.
                    pick: {
                        id: '#picker_' + $input_file_name,
                        multiple: $multiple
                    },
                    // 文件限制大小
                    fileSingleSizeLimit: $size,
                    // 只允许选择文件文件。
                    accept: {
                        title: 'Files',
                        extensions: $ext
                    },
                    // 自定义参数
                    formData: {
                        sizelimit:$sizelimit,
                        extlimit:$ext
                    }
                });
                element.on('tab', function(data){
                    uploader.refresh();
                });
                // 当有文件添加进来的时候
                uploader.on('fileQueued', function(file) {
                    var $li = '<tr id="' + file.id + '" class="file-item"><td>' + file.name + '</td>' +
                    '<td class="file-state">正在读取文件信息...</td><td><div class="layui-progress"><div class="layui-progress-bar" lay-percent="0%"></div></div></td>' +
                    '<td><a href="javascript:void(0);" class="layui-btn download-file layui-btn layui-btn-xs">下载</a> <a href="javascript:void(0);" class="layui-btn remove-file layui-btn layui-btn-xs layui-btn-danger">删除</a></td></tr>';
                    if ($multiple) {
                        $file_list.find('.file-box').append($li);
                    } else {
                        $file_list.find('.file-box').html($li);
                        // 清空原来的数据
                        $input_file.val('');
                    }
                    // 设置当前上传对象
                    curr_uploader = uploader;
                });
                // 文件上传成功
                uploader.on('uploadSuccess', function(file, response) {
                    var $li = $('#' + file.id);
                    if (response.code == 0) {
                        if ($multiple) {
                            if ($input_file.val()) {
                                $input_file.val($input_file.val() + ',' + response.id);
                            } else {
                                $input_file.val(response.id);
                            }
                            $li.find('.remove-file').attr('data-id', response.id);
                        } else {
                            $input_file.val(response.id);
                        }
                    }
                    // 加入提示信息
                    $li.find('.file-state').html('<span class="text-' + response.class + '">' + response.info + '</span>');
                    // 添加下载链接
                    $li.find('.download-file').attr('href', response.path);
                });
                // 文件上传过程中创建进度条实时显示。
                uploader.on('uploadProgress', function(file, percentage) {
                    var $percent = $('#' + file.id).find('.layui-progress-bar');
                    $percent.css('width', percentage * 100 + '%');
                });
                // 文件上传失败，显示上传出错。
                uploader.on('uploadError', function(file) {
                    var $li = $('#' + file.id);
                    $li.find('.file-state').html('<span class="text-danger">服务器发生错误~</span>');
                });
                // 文件验证不通过
                uploader.on('error', function(type) {
                    switch (type) {
                        case 'Q_TYPE_DENIED':
                            layer.alert('图片类型不正确，只允许上传后缀名为：' + $ext + '，请重新上传！', { icon: 5 })
                            break;
                        case 'F_EXCEED_SIZE':
                            layer.alert('图片不得超过' + ($size / 1024) + 'kb，请重新上传！', { icon: 5 })
                            break;
                    }
                });
                // 删除文件
                $file_list.delegate('.remove-file', 'click', function() {
                    if ($multiple) {
                        var id = $(this).data('id'),
                        ids = $input_file.val().split(',');
                        if (id) {
                            for (var i = 0; i < ids.length; i++) {
                                if (ids[i] == id) {
                                    ids.splice(i, 1);
                                    break;
                                }
                            }
                            $input_file.val(ids.join(','));
                        }
                    } else {
                        $input_file.val('');
                    }
                    $(this).closest('.file-item').remove();
                });
                // 将上传实例存起来
                webuploaders.push(uploader);
            });  
        })
    }
    // 绑定fieldlist组件
    if ($(".layui-form .fieldlist").size() > 0) {
        layui.define('laytpl', function(exports) {
            var laytpl = layui.laytpl;
            // 刷新隐藏textarea的值
            var refresh = function(name,obj=null) {
                var data = {};
                var textarea = $("textarea[name='" + name + "']");
                var container = $(".fieldlist[data-name='" + name + "']");
                var template = container.data("template");
                $.each($("input,select,textarea", container).serializeArray(), function(i, j) {
                    var reg = /\[(\w+)\]\[(\w+)\]$/g;
                    var match = reg.exec(j.name);
                    if (!match)
                        return true;
                    match[1] = "x" + parseInt(match[1]);
                    if (typeof data[match[1]] == 'undefined') {
                        data[match[1]] = {};
                    }
                    if(obj!==null && $(obj.elem).attr('name')==j.name){
                        data[match[1]][match[2]] = obj.value;
                    }else{
                        data[match[1]][match[2]] = j.value;
                    }
                });
                var result = template ? [] : {};
                $.each(data, function(i, j) {
                    if (j) {
                        if (!template) {
                            if (j.key != '') {
                                result[j.key] = j.value;
                            }
                        } else {
                            result.push(j);
                        }
                    }
                });
                // console.log(result);
                textarea.val(JSON.stringify(result));
            };
            // 监听文本框改变事件
            $(document).on('change keyup changed', ".fieldlist input,.fieldlist textarea,.fieldlist select", function() {
                refresh($(this).closest(".fieldlist").data("name"));
            });
            form.on('radio(fieldlist)', function(data){
                refresh($(this).closest(".fieldlist").data("name"),data);
            }); 
            form.on('select(fieldlist)', function(data){
                refresh($(this).closest(".fieldlist").data("name"),data);
            });  
            // 追加控制
            $(".layui-form .fieldlist").on("click", ".btn-append,.append", function(e, row) {
                var container = $(this).closest(".fieldlist");
                // var tagName = container.data("tag") || "dd";
                var index = container.data("index");
                var name = container.data("name");
                var id = container.data("id");
                var template = container.data("template");
                var data = container.data();
                index = index ? parseInt(index) : 0;
                container.data("index", index + 1);
                row = row ? row : {};
                var vars = {
                    lists: [{ 'index': index, 'name': name, 'data': data, 'row': row }]
                };
                laytpl($("#" + id + "Tpl").html()).render(vars, function(html) {
                    $(html).insertBefore($(".arrBox", container));
                });
                form.render();
                // var html = template ? Template(template, vars) : Template.render(Form.config.fieldlisttpl, vars);
                // $(html).insertBefore($(tagName + ":last", container));
                // $(this).trigger("fa.event.appendfieldlist", $(this).closest(tagName).prev());
            });
            // 移除控制
            $(".layui-form .fieldlist").on("click", ".btn-remove", function() {
                var container = $(this).closest(".fieldlist");
                // var tagName = container.data("tag") || "dd";
                $(this).closest($(".rules-item")).remove();
                refresh(container.data("name"));
            });
            // 渲染数据&拖拽排序
            $(".layui-form .fieldlist").each(function() {
                var container = this;
                // var tagName = $(this).data("tag") || "dd";
                $(this).dragsort({
                    // itemSelector: $(".rules-item"),
                    dragSelector: ".btn-dragsort",
                    dragEnd: function() {
                        refresh($(this).closest(".fieldlist").data("name"));
                    },
                    // placeHolderTemplate: '<div style="border:1px #009688 dashed;"></div>'
                });
                var textarea = $("textarea[name='" + $(this).data("name") + "']");
                if (textarea.val() == '') {
                    return true;
                }
                var template = $(this).data("template");
                var json = {};
                try {
                    json = JSON.parse(textarea.val());
                } catch (e) {}
                $.each(json, function(i, j) {
                    $(".btn-append,.append", container).trigger('click', template ? j : {
                        key: i,
                        value: j
                    });
                });
                form.render();
            });
        })
    }
    // kindeditor编辑器集合
    var kindeditors = {};
    // 绑定kindeditor编辑器组件
    if ($(".layui-form .textarea-kindeditor").size() > 0) {
        KindEditor.ready(function(K) {
            $(".layui-form .textarea-kindeditor").each(function() {
                var kindeditor_name = $(this).attr('name');
                kindeditors[kindeditor_name] = K.create('textarea[name="'+kindeditor_name+'"]', {
                    // 指定上传文件的服务器端程序。
                    uploadJson : GV.kindeditor_upload_url,
                    // 指定浏览远程图片的服务器端程序。
                    fileManagerJson : GV.kindeditor_file_manager_url,
                    filterMode : false, //关闭过滤模式
                    wellFormatMode : false, //true时美化HTML数据。false关闭美化
                    newlineTag : "br", //设置回车换行标签，可设置”p”、”br”。数据类型: String 默认值: “p”
                    pasteType : 2, //设置粘贴类型，0:禁止粘贴, 1:纯文本粘贴, 2:HTML粘贴 数据类型: Int 默认值: 2
                    allowFileManager : true, //true时显示浏览远程服务器按钮。
                    // 改变站内本地URL，可设置空、relative、absolute、domain。空为不修改URL，relative为相对路径，absolute为绝对路径，domain为带域名的绝对路径。数据类型：String
                    // 默认值：空
                    // 注: 3.4版本开始支持，3.4.1版本开始默认值为空。
                    // urlType:'domain',
                    // 编辑器的宽度，可以设置px或%，比textarea输入框样式表宽度优先度高。数据类型: String 默认值: textarea输入框的宽度
                    width : '100%',
                    // 编辑器的高度，只能设置px，比textarea输入框样式表高度优先度高。数据类型: String 默认值: textarea输入框的高度
                    height : '350px',
                    afterUpload:function(){ this.sync(); }, //图片上传后，将上传内容同步到textarea中
                    afterBlur:function(){ this.sync(); }, //解决KindEditor编辑器Ajax表单提交时获取不到值问题
                    // 编辑器工具
                    items:[
                        'source', '|', 'undo', 'redo', '|', 'preview', 'print', 'template', 'code', 'cut', 'copy', 'paste',
                        'plainpaste', 'wordpaste', '|', 'justifyleft', 'justifycenter', 'justifyright',
                        'justifyfull', 'insertorderedlist', 'insertunorderedlist', 'indent', 'outdent', 'subscript',
                        'superscript', 'clearhtml', 'quickformat', 'selectall', '|', 'fullscreen', '/',
                        'formatblock', 'fontname', 'fontsize', '|', 'forecolor', 'hilitecolor', 'bold',
                        'italic', 'underline', 'strikethrough', 'lineheight', 'removeformat', '|', 'image', 'multiimage',
                        'media', 'insertfile', 'table', 'hr', 'emoticons', 'baidumap', 'pagebreak',
                        'anchor', 'link', 'unlink', '|', 'about'
                    ]
                });
                prettyPrint();
            });
        });
    }
    // ueditor编辑器集合
    var ueditors = {};
    // 绑定ueditor编辑器组件
    if ($(".layui-form .textarea-ueditor").size() > 0) {
        layui.define('ueditor', function(exports) {
            var ueditor = layui.ueditor;
            $('.layui-form .textarea-ueditor').each(function() {
                var ueditor_name = $(this).attr('id');
                ueditors[ueditor_name] = UE.getEditor(ueditor_name, {
                    initialFrameWidth:'100%',
                    initialFrameHeight: 400, //初始化编辑器高度,默认320
                    autoHeightEnabled: false, //是否自动长高
                    maximumWords: 50000, //允许的最大字符数
                    autoFloatEnabled:false,//是否保持toolbar的位置不动,默认true
                    serverUrl: GV.ueditor_upload_url,
                });
                // 图片本地化
                $('#' + ueditor_name + 'grabimg').click(function() {
                    var con = ueditors[ueditor_name].getContent();
                    $.post(GV.ueditor_grabimg_url, { 'content': con, 'type': 'images' },
                    function(data) {
                        ueditors[ueditor_name].setContent(data);
                        layer.msg("图片本地化完成");
                    }, 'html');
                });
                // 过滤敏感字
                $('#' + ueditor_name + 'filterword').click(function() {
                    var con = ueditors[ueditor_name].getContent();
                    $.post(GV.filter_word_url, { 'content': con }).success(function(res) {
                        if (res.code == 0) {
                            if ($.isArray(res.data)) {
                                layer.msg("违禁词：" + res.data.join(","), { icon: 2 });
                            }
                        } else {
                            layer.msg("内容没有违禁词！", { icon: 1 });
                        }
                    })
                })
            });
        })
    }
    // editormd编辑器集合
    var editormds = {};
    // 绑定editormd编辑器组件
    if ($(".js-editormd").size() > 0) {
        $('.js-editormd').each(function() {
            var editormd_name = $(this).attr('id');
            editormds[editormd_name] = editormd(editormd_name, {
                height: 500, // 高度
                watch: true,
                searchReplace: true,
                emoji: false,
                toolbarAutoFixed: false, // 取消工具栏固定
                path: GV.editormd_path_url, // 用于自动加载其他模块
                codeFold: true, // 开启代码折叠
                dialogLockScreen: false, // 设置弹出层对话框不锁屏
                imageUpload: true, // 开启图片上传
                imageUploadURL: GV.editormd_upload_url,
                onload : function() {
                    var a = this;
                    /* setTimeout(function () {
                        a.watch(false);
                    }, 900); */
                    setInterval(function(){ a.watch(false); }, 900);
                }
            });
        });
    }
    // 裁剪图片
    $(document).on('click', '.cropper', function() {
        var inputId = $(this).attr("data-input-id");
        var image = $(this).closest(".thumbnail").children('img').data('original');
        var dataId = $(this).data("id");
        var index = layer.open({
            type: 2,
            shadeClose: true,
            shade: false,
            area: ['880px', '620px'],
            title: '图片裁剪',
            content: GV.jcrop_upload_url + '?url=' + image,
            success: function(layero, index) {
                $(layero).data("arr", [inputId, dataId]);
            }
        });
    });
    // 参数点击复制
    $('.zerocopy').on('click', function () {
        var value = $(this).text();
        $(this).attr('data-clipboard-text', value);
        var c_lipboard = new clipboard('.zerocopy');
        c_lipboard.on('success', function (e) {
            layer.msg("复制成功",{ icon: 1,time:1500});
            c_lipboard.destroy();
        });
        c_lipboard.on('error', function (e) {
            layer.msg("复制失败！请手动调用",{ icon: 2,time:1500});
            c_lipboard.destroy();
        });
    });
    $(".zerocopy").mouseover(function(){
        $(this).css("color","blue");
    });
    $(".zerocopy").mouseout(function(){
        $(this).css("color","");
    });
    // 数组点击软删除
    window.delinfo = function(o){
		$(o).parent().parent().parent().remove();
	}
	// 数组点击追加
	$(".btn-append").click(function(){
		var div = $(this).prev().children().last();
		var newdiv=div.clone(true);
		newdiv.find("input").val('');
		newdiv.find(".sysval").html('');
		$(this).prev().append(newdiv);
	});
	// 数组拖动
	$(".array-move").sortable({
		// 当排序动作开始时触发此事件
		start: function(event, ui) {
			// console.log(event);
			// console.log(ui);
		},
		// 当排序动作结束时触发此事件
		stop: function(event, ui) {
			// console.log(event);
			// console.log(ui);
			// console.log(this);
			// var filename = $(this).attr('id');
			// var arr = new Array();
			// $(this).find('button').each(function (index, el) {
			// 	console.log($(el).attr('filevalue'));
			// 	arr.push($(this).attr('filevalue'));
			// });
			// var str = arr.join(',');
			// // console.log(str);
			// $("input[name=" + filename + "]").val(str);
		},
    });
    // 地区选择，第一种方法
    /* window.changeCity = function(that)
    {
		var selid =  that.attr('id');
		var pid = that.val();
		var strsplit = selid.split("_") ;
		var setCity  = strsplit[0].toString();
		var fields  = strsplit[1].toString();
		if(setCity=='province'){
			$('#city_'+fields+' option:gt(0)').remove();
			$('#area_'+fields+' option:gt(0)').remove();
			$('input[name="'+fields+'"]').val(pid);
			setCity = 'city_'+fields;
		}else if (setCity=='city') {
			setCity = 'area_'+fields;
			var val = $('input[name="'+fields+'"]').val();
			var valstr = val.split(",") ;
			var valprovince  = valstr[0].toString();
			$('input[name="'+fields+'"]').val(valprovince+','+pid);
		}else{
			var val = $('input[name="'+fields+'"]').val();
			var valstr = val.split(",") ;
			var valprovince  = valstr[0].toString();
			var valcity  = valstr[1].toString();
			$('input[name="'+fields+'"]').val(valprovince+','+valcity+','+pid);
		}
		$.getJSON(GV.get_city,{"pid":pid},function(data){
			if(data){
				var str='<option value="" >请选择  </option>';
				for(var i in data)
				{
					if (data[i].id) {
						str+="<option value="+data[i].id+">"+data[i].shortname+"</option>";
					}
				}
				$("#"+setCity+"").html(str);
			}
		});
	} */
	// 地区选择，第二种方法
    window.changeCity = function(that)
    {
		var selid =  that.attr('id');
		var pid = that.val();
		var strsplit = selid.split("_") ;
		var setCity  = strsplit[0].toString();
		var fields  = strsplit[1].toString();
		if(setCity=='province'){
			$('#city_'+fields+' option:gt(0)').remove();
			$('#area_'+fields+' option:gt(0)').remove();
			$('input[name="'+fields+'"]').val(pid);
			setCity = 'city_'+fields;
		}else if (setCity=='city') {
			setCity = 'area_'+fields;
			var val = $('input[name="'+fields+'"]').val();
			var valstr = val.split(",") ;
			var valprovince  = valstr[0].toString();
			$('input[name="'+fields+'"]').val(valprovince+','+pid);
		}else{
			var val = $('input[name="'+fields+'"]').val();
			var valstr = val.split(",") ;
			var valprovince  = valstr[0].toString();
			var valcity  = valstr[1].toString();
			$('input[name="'+fields+'"]').val(valprovince+','+valcity+','+pid);
		}
		$.getJSON(GV.get_city,{"pid":pid},function(data){
			if(data){
				var str='<option value="" >请选择  </option>';
				for(var i in data) {
					if (data[i].id) {
						str+="<option value="+data[i].id+">"+data[i].shortname+"</option>";
					}
				}
				$("#"+setCity+"").html(str);
				xuanran();
			}
		});
    }
    // 地图字段搜索地图
    window.theLocation = function(name,mapObj)
    {
		var city = $("select[name='"+name+"']").find("option:selected").text();
		if(city == "请选择县/区"){
			layer.msg('请选择县/区', {icon: 2,time:1500});
		}else{
			// 用城市名设置地图中心点
			mapObj.centerAndZoom(city,11);
		}
	}
	// 地图字段搜索地址
    window.baiduSearchAddress = function(mapObj, name, level)
    {
		var address = $('#baidu_address_'+name).val();
		if ( address.indexOf(",") != -1 && address.indexOf(".") != -1) {
			// 表示坐标
			var data = address.split(',');
			var lngX = data[0];
			var latY = data[1];
			var zoom = 17;
			mapObj.centerAndZoom(new BMap.Point(lngX,latY),zoom);
			// 创建图标对象
			var myIcon = new BMap.Icon('__STATIC__/common/images/mak.png', new BMap.Size(27, 45));
			// 创建标注对象并添加到地图
			var center = mapObj.getCenter();
			var point = new BMap.Point(lngX,latY);
			var marker = new BMap.Marker(point, {icon: myIcon});
			marker.enableDragging();
			mapObj.addOverlay(marker);
			var ZoomLevel = mapObj.getZoom();
			$('#dr_'+name).val(address);
			mapObj.addEventListener("click",function(e){
				$('#dr_'+name).val(e.point.lng+','+e.point.lat);
				marker.point.lng = e.point.lng;
				marker.point.lat = e.point.lat;
				var point2 = new BMap.Point(e.point.lng,e.point.lat);
				var marker2 = new BMap.Marker(point2);// 创建标注
				mapObj.clearOverlays();
				mapObj.addOverlay(marker2);
				marker.disableDragging();
			});
		} else {
			/* var myGeo = new BMap.Geocoder();
			// 将地址解析结果显示在地图上,并调整地图视野
			myGeo.getPoint(address, function(point){
				if (point) {
					mapObj.centerAndZoom(point, 13);
					mapObj.addOverlay(new BMap.Marker(point));
				}else{
					dr_tips(0, "没有找到这个地址");
				}
			});
			// mapObj.setCenter(address); */
			var myGeo = new BMap.Geocoder();
			// 将地址解析结果显示在地图上,并调整地图视野
			myGeo.getPoint(address, function(point){
				if (point) {
					mapObj.centerAndZoom(point, 13);
					// mapObj.addOverlay(new BMap.Marker(point));
					// 创建图标对象
					var myIcon = new BMap.Icon('__STATIC__/common/images/mak.png', new BMap.Size(27, 45));
					// 创建标注对象并添加到地图
					var center = mapObj.getCenter();
					var point = new BMap.Point(lngX,latY);
					var marker = new BMap.Marker(point, {icon: myIcon});
					marker.enableDragging();
					mapObj.addOverlay(marker);
					var ZoomLevel = mapObj.getZoom();
					$('#dr_'+name).val(address);
					mapObj.addEventListener("click",function(e){
						$('#dr_'+name).val(e.point.lng+','+e.point.lat);
						marker.point.lng = e.point.lng;
						marker.point.lat = e.point.lat;
						var point2 = new BMap.Point(e.point.lng,e.point.lat);
						var marker2 = new BMap.Marker(point2);// 创建标注
						mapObj.clearOverlays();
						mapObj.addOverlay(marker2);
						marker.disableDragging();
					});
				}else{
					dr_tips(0, "没有找到这个地址");
				}
			});
			// mapObj.setCenter(address);
		}
    }
    // 联动选择，第一种方法
    /* window.changeStep = function(that)
    {
		var selid =  that.attr('id');
		var pid = that.val();
		var steplist = selid.split("_") ;
		var fields  = steplist[1].toString();
		$(that).nextAll().remove();
		if (pid) {
			$.getJSON("{:url('cms.Select/getStep')}",{"pid":pid},function(data){
				if(data.length !=0){
					var str='<select name="" id="step_'+fields+'_'+pid+'"><option value="" >请选择  </option>';
					for(var i in data)
					{
						if (data[i].id) {
							str+="<option value="+data[i].id+">"+data[i].title+"</option>";
						}
					}
					str+="</select>";
					$(that).nextAll().remove();
					$(that).parent().append(str);
				}
			});
		}
		var setValue = '';
		$('#div_'+fields+' select :selected').each(function() {
			if (setValue=='') {
				setValue = $(this).val();
			}else{
				setValue += ','+$(this).val();
			}
		});
		$('input[name="'+fields+'"]').val(setValue);
	} */
	// 联动选择，第二种方法
    window.changeStep = function(that)
    {
		var selid =  that.attr('id');
		var pid = that.val();
		var steplist = selid.split("_") ;
		var fields  = steplist[1].toString();
		$(that).parent().nextAll().remove();
		if (pid) {
			$.getJSON(GV.select_get_step_url,{"pid":pid},function(data){
				if(data.length !=0){
					var str = '<div class="layui-input-inline stepselect" id="div_'+fields+'">';
					str+='<select name="" id="step_'+fields+'_'+pid+'"lay-filter="div_'+fields+'"><option value="" >请选择  </option>';
					for(var i in data) {
						if (data[i].id) {
							str+="<option value="+data[i].id+">"+data[i].title+"</option>";
						}
					}
					str+="</select>";
					str+="</div>";
					$(that).parent().nextAll().remove();
					$(that).parent().parent().append(str);
					xuanran("select");
				}
			});
		}
		var setValue = '';
		$('#div_'+fields+' select :selected').each(function() {
			if (setValue=='') {
				setValue = $(this).val();
			}else{
				setValue += ','+$(this).val();
			}
		});
		$('input[name="'+fields+'"]').val(setValue);
    }
    window.changeSelected = function(fields)
	{
		var stepval  = $('input[name="'+fields+'"]').val();
		var steplist = stepval.split(",");
		var that = '#step_'+fields+'_0';
		console.log(steplist);
		if(steplist.length !=0){
			for(var i = 0; i < steplist.length; i++)
			{
				var pid  =steplist[i];
				var tid = steplist[(i+1)];
				if (pid) {
					$.ajax({
						url : GV.select_get_step_url,
						type : "POST",
						async:false,
						data : {"pid":pid },
						success : function(data)
						{
							if(data.length !=0){
								var str = '<div class="layui-input-inline stepselect" id="div_'+fields+'">';
								str+='<select name="" id="step_'+fields+'_'+pid+'"><option value="" >请选择  </option>';
								for(var i in data)
								{
									if (data[i].id) {
										if (tid && tid==data[i].id) {
											str+="<option value="+data[i].id+" selected>"+data[i].title+"</option>";
										}else{
											str+="<option value="+data[i].id+">"+data[i].title+"</option>";
										}

									}
								}
								str+="</select>";
								str+="</div>";
								$(that).parent().nextAll().remove();
								$(that).parent().parent().append(str);
								that = '#step_'+fields+'_'+pid;
								xuanran();
							}
						},
						error : function()
						{
							alert("请求错误")
						}
					});
				}
			}
		}
	}
    exports(MOD_NAME, obj);
});