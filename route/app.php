<?php
// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006~2018 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: liu21st <liu21st@gmail.com>
// +----------------------------------------------------------------------
use think\facade\Route;
/*
Route::get('think', function () {
    return 'hello,ThinkPHP6!';
});

Route::get('hello/:name', 'index/hello');
*/
//cms路由设置
if(app('http')->getName() == '' && !isAddonInstall('cms')) {
    echo '<style type="text/css">*{ padding: 0; margin: 0; } div{ padding: 4px 48px;} a{color:#2E5CD5;cursor: pointer;text-decoration: none} a:hover{text-decoration:underline; } body{ background: #fff; font-family: "Century Gothic","Microsoft yahei"; color: #333;font-size:18px;} h1{ font-size: 100px; font-weight: normal; margin-bottom: 12px; } p{ line-height: 1.6em; font-size: 42px }</style><div style="padding: 24px 48px;"> <h1>:) </h1><p> ZengCMS V' . ZENGCMS_VERSION . '<br/><span style="font-size:30px">你值得信赖的PHP框架</span></p></div>';die;
}
if(app('http')->getName() == '' && isAddonInstall('cms')) {
    $cms_config = get_addon_config('cms');
    // 首页路由
    Route::get('[:lang]/index$', 'Index/index')
    ->ext($cms_config['index_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG']]);
    // 搜索列表路由
    Route::get('[:lang]/[:city]/search', 'Search/index')
    ->ext($cms_config['other_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+']);
    // tag某标签所有信息分页路由
    Route::get('[:lang]/[:city]/tag/:tag/[:page]', 'Tag/detail')
    ->ext($cms_config['other_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+','page'=>'\d+']);
    // tag所有标签分页路由?page=x&keywords=x
    Route::get('[:lang]/[:city]/tag', 'Tag/index')
    ->ext($cms_config['other_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+']);
    // 下载路由
    Route::get('[:lang]/[:city]/download', 'Download/index')
    ->ext($cms_config['other_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+']);
    // 栏目分页访问路由-注意 id_
    Route::get('[:lang]/[:city]/:id/list_:id_:page', 'Category/index')
    ->ext($cms_config['category_page_suffix']?:'html')
    ->pattern([
        'lang'=>$cms_config['WEB_INDEX_LANG'],
        'city' => '\w+','id' => get_all_category_name(),
        'id_'=>'[0-9]+_','page'=>'\d+'
    ]);
    // 内容详情路由
    Route::get('[:lang]/[:city]/:catename/:id', 'Article/index')
    ->ext($cms_config['article_suffix']?:'html')
    ->pattern([
        'lang'=>$cms_config['WEB_INDEX_LANG'],
        'city' => '\w+',
        'catename'=>get_all_category_name(),
        'id' => '\d+'
    ]);
    // 栏目不分页访问路由
    if($cms_config['category_suffix']){
        Route::get('[:lang]/[:city]/:id', 'Category/index')
        ->ext($cms_config['category_suffix'])
        ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+','id' => get_all_category_name()]);
    }else{
        Route::get('[:lang]/[:city]/:id/', 'Category/index')
        ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+','id' => get_all_category_name()]);
    }
    // 模板演示路由
    Route::get('[:lang]/[:city]/demo/:cid_:id', 'Demo/index')
    ->ext($cms_config['other_suffix']?:'html')
    ->pattern(['lang'=>$cms_config['WEB_INDEX_LANG'],'city' => '\w+','cid_'=>'[0-9]+_','id' => '[0-9]+']);
}