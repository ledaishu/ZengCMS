<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 图片处理工具类
// +----------------------------------------------------------------------
namespace imagetool;

use think\Image;

class ImageTool
{
    /**
	 * [thumb 缩略图]
	 * @param  [type] $dst    		 [目标图片]
	 * @param  [type] $type   		 [缩略图类型，1:等比例缩,2:缩放后填充,3:居中裁剪,4:左上角裁剪,5:右下角裁剪,6:固定尺寸缩放]
	 * @param  [type] $width  	  	 [缩略图宽度]
	 * @param  [type] $height 		 [缩略图高度]
	 * @param null|string $save_type [图像保存类型]
	 * @param int  $quality   		 [图像质量]
	 * @param bool $interlace 		 [是否对JPEG类型图像设置隔行扫描，1:是，0：否]
	 * @param  [type] $del    		 [是否删除原图]
	 * @return [type]          		 [返回缩略图路径]
	 */
	public static function thumb($dst, $type = '', $width = '', $height = '', $save_type = '', $quality = '', $interlace = '', $del = false)
	{
		// 缩略图类型1:等比例缩,2:缩放后填充,3:居中裁剪,4:左上角裁剪,5:右下角裁剪,6:固定尺寸缩放
		$type = intval($type);
		if (!$type) {
			$type = sys_config('WEB_THUMB_TYPE',1);
		}
		// 缩略图宽度
		$width = intval($width);
		if (!$width) {
			$width = sys_config('WEB_THUMB_WIDTH',150);
		}
		// 缩略图高度
		$height = intval($height);
		if (!$height) {
			$height = sys_config('WEB_THUMB_HEIGHT',150);
		}
		// 图像保存类型 png jpeg jpg gif
		if (!$save_type) {
			$save_type = sys_config('WEB_THUMB_SAVE_TYPE',null);
		}
		// 图像质量
		$quality = intval($quality);
		if (!$quality) {
			$quality = sys_config('WEB_THUMB_QUALITY',80);
		}
		// 是否对JPEG类型图像设置隔行扫描
		if ($interlace == '') {
			$interlace = sys_config('WEB_THUMB_INTERLACE');
		}
		if ($interlace) {
			$interlace = true;
		} else {
			$interlace = false;
		}
		$array = explode('.', basename($dst));
		// 新图片根路径
		$new_img_path = dirname($dst) . '/' . md5((string) microtime(true)) . '.' . end($array);
		$image = Image::open($dst);
		$image->thumb($width, $height, $type)->save($new_img_path, $save_type, $quality, $interlace);
		if ($del) {
			@unlink($dst);
		}
		return ['code' => 1, 'msg' => '缩略成功！', 'path' => str_replace(STATIC_PATH . '/', '', $new_img_path)];
	}
    /**
	 * [water 水印图片设置]
	 * @param  [type] $dst        [原图]
	 * @param  [type] $type       [水印类型 1：图片，2：文字]
	 * @param  [type] $water_img  [水印图片]
	 * @param  [type] $water_text [水印文字]
	 * @param  [type] $text_size  [水印文字大小]
	 * @param  [type] $text_color [水印文字颜色]
	 * @param  [type] $water_tmd  [水印透明度]
	 * @param  [type] $water_pos  [水印位置]
	 * @param  [type] $del        [是否删除原图]
	 * @return [type]             [description]
	 */
	public static function water($dst, $type = '', $water_img = '', $water_text = '', $text_size = '', $text_color = '', $water_tmd = '', $water_pos = '', $del = false)
	{
		// 水印类型1:图片,2:文字
		$type = intval($type);
		if (!$type) {
			$type = sys_config('WEB_WATER_TYPE',2);
		}
		// 水印透明度
		if (!$water_tmd) {
			$water_tmd = sys_config('WEB_WATER_TMD',50);
		}
		// 水印位置
		if (!$water_pos) {
			$water_pos = sys_config('WEB_WATER_POS',8);
		}
		$array = explode('.', basename($dst));
		// 新图片根路径
		$new_img_path = dirname($dst) . '/' . md5((string) microtime(true)) . '.' . end($array);
		if ($type == 1) { //图片水印
			if (!$water_img) {
				$water_img = get_file_path(sys_config('WEB_WATER_IMG'),2);
			}
			if(stripos($water_img,'http') !== false){
				$_water_img = STATIC_PATH . '/uploads/temp/waterimg.png';
				mk_dirs(dirname($_water_img));
				@unlink($_water_img);
				if (http_down($water_img, $_water_img) !== false) {
					$water_img = $_water_img;
				}
			}
			$image = Image::open($dst);
			$image->water($water_img, $water_pos, $water_tmd)->save($new_img_path);
		} else { //文字水印
			// 文字
			if (!$water_text) {
				$water_text = sys_config('WEB_WATER_TEXT','zengcms');
			}
			// 文字大小
			$text_size = intval($text_size);
			if (!$text_size) {
				$text_size = sys_config('WEB_WATER_TEXT_SIZE',20);
			}
			// 文字颜色
			if (!$text_color) {
				$text_color = sys_config('WEB_WATER_TEXT_COLOR','#ffffff');
			}
			$image = Image::open($dst);
			$image->text($water_text, PROJECT_PATH . '/data/msyh.ttc', $text_size, $text_color, $water_pos)->save($new_img_path);
		}
		// 判断是否删除原图
		if ($del) {
			@unlink($dst);
		}
		return ['code' => 1, 'msg' => '水印成功！', 'path' => str_replace(STATIC_PATH . '/', '', $new_img_path)];
	}
}
