<?php
namespace addons\cms;

use think\Addons;

class Plugin extends Addons
{
    // 该插件的基础信息
    public $info = [
        'name' => 'cms',
        'title' => 'cms内容管理',
        'description' => '这是一个功能强大的内容管理插件！',
        'status' => 1,
        'author' => 'ZengCMS',
        'require' => '1.0.0',
        'version' => '1.0.0',
        'website' => '',
        'images'=>'addons/cms/images/cms.jpg',
        'group'=>'',
        'is_hook'=>0,
        // 依赖插件
        'need_plugin' => [
            // ['member','1.0.0','>='],
        ],
        // 数据表，不要加表前缀[有数据库表时必填]
        'tables' => [
            'model',
            'document',
            'document_article',
            'download',
            'image',
            'message',
            'attribute',
            'arctype',
            'arctype_priv',
            'stepselect',
            'tag',
            'tagmap',
            'debris_pos',
            'debris',
            'adtype',
            'advert',
            'link_type',
            'links',
            'product',
        ],
    ];
    public $menu = [
        'is_nav' => 1,//1导航栏,0非导航栏
        'pid'    =>0,//当is_nav为1时可设置,默认为0
        'menu' => [
            'title' => 'CMS',
            'title_en' => 'CMS',
            'name' => 'admin/Cms',
            'type' => 1,
            'condition' => '',
            'status' => 1,
            'show' => 1,
            'icon' => 'laptop',
            'remark' => 'CMS管理',
            'sort' => 80,
            'menulist' => 
            array (
                0 => 
                array (
                'title' => '友情链接',
                'title_en' => 'Friendship Links',
                'name' => 'admin/cms.Links/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'link',
                'remark' => '',
                'sort' => 100,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '链接类型',
                    'title_en' => 'Link Type',
                    'name' => 'admin/cms.LinkType/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.LinkType/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 1,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.LinkType/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 1,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.LinkType/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 1,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '排序',
                        'title_en' => '',
                        'name' => 'admin/cms.LinkType/sort',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 1,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                    1 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Links/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Links/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Links/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Links/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    5 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Links/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                1 => 
                array (
                'title' => '广告管理',
                'title_en' => 'Advert Manager',
                'name' => 'admin/cms.Advert/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'audio-description',
                'remark' => '',
                'sort' => 90,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '广告位',
                    'title_en' => 'Adtype',
                    'name' => 'admin/cms.Adtype/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.Adtype/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.Adtype/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.Adtype/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '排序',
                        'title_en' => '',
                        'name' => 'admin/cms.Adtype/sort',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                    1 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Advert/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Advert/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Advert/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Advert/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    5 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Advert/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                2 => 
                array (
                'title' => '碎片管理',
                'title_en' => 'Debris Manager',
                'name' => 'admin/cms.Debris/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'gift',
                'remark' => '',
                'sort' => 89,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '碎片位',
                    'title_en' => 'DebrisPos',
                    'name' => 'admin/cms.DebrisPos/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 1,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.DebrisPos/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.DebrisPos/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.DebrisPos/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '排序',
                        'title_en' => '',
                        'name' => 'admin/cms.DebrisPos/sort',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                    1 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Debris/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Debris/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Debris/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Debris/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    5 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Debris/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                3 => 
                array (
                'title' => 'TAG管理',
                'title_en' => 'TAG Manager',
                'name' => 'admin/cms.Tag/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'tag',
                'remark' => 'TAG标签管理',
                'sort' => 88,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Tag/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    1 => 
                    array (
                    'title' => '重建',
                    'title_en' => '',
                    'name' => 'admin/cms.Tag/rebuilt',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '数据重建',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Tag/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Tag/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Tag/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                4 => 
                array (
                'title' => '栏目管理',
                'title_en' => 'Arctype Manager',
                'name' => 'admin/cms.Arctype/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'list',
                'remark' => '',
                'sort' => 84,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    1 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    5 => 
                    array (
                    'title' => '中文转拼音',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/get_pinyin',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    6 => 
                    array (
                    'title' => '编辑内容',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/edit_content',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    7 => 
                    array (
                    'title' => '展示 / 折叠',
                    'title_en' => '',
                    'name' => 'admin/cms.Arctype/arctype_show',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                5 => 
                array (
                'title' => '管理内容',
                'title_en' => 'Manager Content',
                'name' => 'admin/cms.Document/content',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'clipboard',
                'remark' => '',
                'sort' => 82,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '左侧栏目',
                    'title_en' => 'Arctype',
                    'name' => 'admin/cms.Document/arctype',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 100,
                    ),
                    1 => 
                    array (
                    'title' => '右侧面板',
                    'title_en' => 'Panl',
                    'name' => 'admin/cms.Document/panl',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 90,
                    ),
                    2 => 
                    array (
                    'title' => '右侧文档',
                    'title_en' => 'Document',
                    'name' => 'admin/cms.Document/document',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => 'file',
                    'remark' => '',
                    'sort' => 83,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '状态',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/set_status',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        4 => 
                        array (
                        'title' => '排序',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/set_sort',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        5 => 
                        array (
                        'title' => '移动',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/move',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        6 => 
                        array (
                        'title' => '复制',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/copy',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        7 => 
                        array (
                        'title' => '左侧菜单',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/public_arctype',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        8 => 
                        array (
                        'title' => '右侧内容',
                        'title_en' => '',
                        'name' => 'admin/cms.Document/public_document',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                ),
                ),
                6 => 
                array (
                'title' => '优化设置',
                'title_en' => 'Optimize settings',
                'name' => 'admin/cms.Seoset',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'wrench',
                'remark' => '',
                'sort' => 81,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '首页优化',
                    'title_en' => 'Index Optimization',
                    'name' => 'admin/cms.Seoset/site',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 100,
                    ),
                    1 => 
                    array (
                    'title' => '栏目优化',
                    'title_en' => 'Arctype Optimization',
                    'name' => 'admin/cms.Seoset/category',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 90,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '编辑栏目',
                        'title_en' => '',
                        'name' => 'admin/cms.Seoset/editCategory',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                    2 => 
                    array (
                    'title' => '地区优化',
                    'title_en' => 'Regional Optimization',
                    'name' => 'admin/cms.Seoset/area',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 60,
                    ),
                    3 => 
                    array (
                    'title' => '长尾词优化',
                    'title_en' => 'Long-tailed Word Optimization',
                    'name' => 'admin/cms.Seoset/longKey',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '优化操作',
                    'title_en' => 'Optimizing Operation',
                    'name' => 'admin/cms.Seoset/operate',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                7 => 
                array (
                'title' => '更新管理',
                'title_en' => 'Update Manager',
                'name' => 'admin/cms.Update',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'paper-plane',
                'remark' => '',
                'sort' => 80,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '更新主页HTML',
                    'title_en' => 'Update The Main HTML',
                    'name' => 'admin/cms.Update/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    1 => 
                    array (
                    'title' => '更新栏目HTML',
                    'title_en' => 'Update Arctype HTML',
                    'name' => 'admin/cms.Update/category',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '更新文档HTML',
                    'title_en' => 'Update Document HTML',
                    'name' => 'admin/cms.Update/document',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                8 => 
                array (
                'title' => '表单管理',
                'title_en' => 'Form Manager',
                'name' => 'admin/cms.Form/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'calendar',
                'remark' => '',
                'sort' => 70,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    1 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    5 => 
                    array (
                    'title' => '信息列表',
                    'title_en' => '',
                    'name' => 'admin/cms.Form/info',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
                9 => 
                array (
                'title' => '模型管理',
                'title_en' => 'Model Management',
                'name' => 'admin/cms.ModelManager',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 1,
                'icon' => 'maxcdn',
                'remark' => '',
                'sort' => 60,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '模型列表',
                    'title_en' => 'Model List',
                    'name' => 'admin/cms.Model/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 20,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.Model/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.Model/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.Model/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '状态',
                        'title_en' => '',
                        'name' => 'admin/cms.Model/setStatus',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        4 => 
                        array (
                        'title' => '排序',
                        'title_en' => '',
                        'name' => 'admin/cms.Model/sort',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                    1 => 
                    array (
                    'title' => '联动类别',
                    'title_en' => 'Select',
                    'name' => 'admin/cms.Select/index',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 1,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    'menulist' => 
                    array (
                        0 => 
                        array (
                        'title' => '新增',
                        'title_en' => '',
                        'name' => 'admin/cms.Select/add',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        1 => 
                        array (
                        'title' => '编辑',
                        'title_en' => '',
                        'name' => 'admin/cms.Select/edit',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        2 => 
                        array (
                        'title' => '删除',
                        'title_en' => '',
                        'name' => 'admin/cms.Select/del',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        3 => 
                        array (
                        'title' => '查看子分类',
                        'title_en' => '',
                        'name' => 'admin/cms.Select/list_select',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                        4 => 
                        array (
                        'title' => '根据pid获取下一级联动信息',
                        'title_en' => '',
                        'name' => 'admin/cms.Select/getStep',
                        'type' => 1,
                        'condition' => '',
                        'status' => 1,
                        'show' => 0,
                        'icon' => '',
                        'remark' => '',
                        'sort' => 0,
                        ),
                    ),
                    ),
                ),
                ),
                10 => 
                array (
                'title' => '字段管理',
                'title_en' => 'Attribute Manager',
                'name' => 'admin/cms.Attribute/index',
                'type' => 1,
                'condition' => '',
                'status' => 1,
                'show' => 0,
                'icon' => '',
                'remark' => '',
                'sort' => 0,
                'menulist' => 
                array (
                    0 => 
                    array (
                    'title' => '新增',
                    'title_en' => '',
                    'name' => 'admin/cms.Attribute/add',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    1 => 
                    array (
                    'title' => '编辑',
                    'title_en' => '',
                    'name' => 'admin/cms.Attribute/edit',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    2 => 
                    array (
                    'title' => '删除',
                    'title_en' => '',
                    'name' => 'admin/cms.Attribute/del',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    3 => 
                    array (
                    'title' => '状态',
                    'title_en' => '',
                    'name' => 'admin/cms.Attribute/setStatus',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                    4 => 
                    array (
                    'title' => '排序',
                    'title_en' => '',
                    'name' => 'admin/cms.Attribute/sort',
                    'type' => 1,
                    'condition' => '',
                    'status' => 1,
                    'show' => 0,
                    'icon' => '',
                    'remark' => '',
                    'sort' => 0,
                    ),
                ),
                ),
            )
        ]
    ];
    /**
     * 插件安装方法
     * @return bool
     */
    public function install()
    {
        return true;
    }
    /**
     * 插件卸载方法
     * @return bool
     */
    public function uninstall()
    {
        return true;
    }
    /**
     * 插件使用方法
     * @return bool
     */
    public function enabled()
    {
        return true;
    }
    /**
     * 插件禁用方法
     * @return bool
     */
    public function disabled()
    {
        return true;
    }
}
