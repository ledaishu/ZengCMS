<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | setting配置
// +----------------------------------------------------------------------
return [
    // 权限类型：1：Auth，2：Node，3：Auth+Node
    'permission_type'=>1,
    // Auth权限配置
    'auth_config'=>[
        'auth_on' => true,//认证开关
		'auth_type' => 1,//认证方式，1为实时认证；2为登录认证。
		'auth_group' => 'auth_group',//用户组数据表名
		'auth_group_access' => 'auth_group_access',//用户-用户组关系表
		'auth_rule' => 'auth_rule',//权限规则表
		'auth_user' => 'admin',//用户信息表
    ],
    // 不需要验证登录的控制器
    'no_login_controller' => [
        'login',
    ],
    // 不需要验证登录的节点
    'no_login_node'       => [
        'login/index',
    ],
    // 不需要验证权限的控制器
    'no_auth_controller'  => [
        'login',
        'common',
        'index',
    ],
    // 不需要验证权限的节点
    'no_auth_node'        => [
        'base/menu',
        'base/clear_cache',
        'base/clear_static',
        'base/ajaxGetCity',
        'base/get_city',
        'admin/logout'
    ],
    // logs日志行为
    'logs'                => [
        'admin/Login/index'       => '后台登录',
        'admin/Common/verify'     => '登录验证码',
        'admin/Admin/logout'      => '退出登录',
        'admin/Base/clear_static' => '清除缓存',
        'admin/Base/clear_static' => '清除静态',
        'admin/Base/menu'         => '菜单接口',
        // 不用记录
        'not_insert_logs'         => [
            'admin/Logs/index',
            '/admin/login.php'
        ],
    ]
];
