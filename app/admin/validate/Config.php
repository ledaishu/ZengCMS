<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 配置验证器
// +----------------------------------------------------------------------
namespace app\admin\validate;

use think\Validate;

class Config extends Validate
{
	protected $rule = [
		'title'  => 'require|max:32|unique:config',
		'name'   => 'require|max:32|unique:config',
		'type'   => 'require',
		'typeid' => 'require|number',
		'remark' => 'max:255',
		'iscore' => 'require|in:0,1',
		'status' => 'require|in:0,1',
		'sort'   => 'require|number',
	];
	protected $message = [
		'title.require' => '配置中文名称必须填写',
		'title.max' => '配置中文名称最多不能超过32个字符',
		'title.unique' => '配置中文名称已经存在',
		'name.require' => '配置英文名称必须填写',
		'name.max' => '配置英文名称最多不能超过32个字符',
		'name.unique' => '配置英文名称已经存在',
		'type.require' => '表单类型必须选择！',
		'typeid.require' => '配置类型必须选择！',
		'typeid.number' => '配置类型选择有误！',
		'remark.max' => '配置说明最多不能超过255个字符',
		'iscore.require' => '核心配置必须选择！',
		'iscore.in' => '核心配置必须是0或1',
		'status.require' => '状态必须选择！',
		'status.in' => '状态必须是0或1',
		'sort.require' => '排序必须填写！',
		'sort.number' => '排序值必须是数字',
	];
	protected $scene = [
		'add' => ['title', 'name', 'type', 'typeid', 'remark', 'iscore', 'status', 'sort'],
		'edit' => ['title', 'name', 'type', 'typeid', 'remark', 'iscore', 'status', 'sort'],
	];
}
