<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 全局验证码处理类
// +----------------------------------------------------------------------
namespace app\api\controller;

use think\facade\Config;
use app\common\controller\Base;
use think\captcha\facade\Captcha as ThinkCaptcha;

class Captcha extends Base
{
    // 获取验证码
    public function getVerify()
    {
        $captcha = [];
        // 验证码位数
        $codelen = $this->request->param('length', 4);
        if ($codelen) {
            if ($codelen > 8 || $codelen < 2) {
                $codelen = 4;
            }
            $captcha['length'] = $codelen;
        }
        // 设置验证码字符为纯数字
        $codeSet = $this->request->param('codeSet');
        if ($codeSet) {
            $captcha['codeSet'] = $codeSet;
        }
        // 关闭验证码杂点
        $captcha['useNoise'] = $this->request->param('useNoise',false);
        // 验证码字体大小(px)
        $fontsize = $this->request->param('fontSize', 15);
        if ($fontsize) {
            $captcha['fontSize'] = $fontsize;
        }
        // 验证码图片高度，设置为0为自动计算
        $height = $this->request->param('imageH', 35);
        if ($height) {
            $captcha['imageH'] = $height;
        }
        // 验证码图片宽度，设置为0为自动计算
        $width = $this->request->param('imageW', 100);
        if ($width) {
            $captcha['imageW'] = $width;
        }
        // 是否画混淆曲线
        $captcha['useCurve'] = $this->request->param('useCurve',true);
        // 使用算术验证码
        $captcha['math'] = $this->request->param('math',false);
        // 验证成功后是否重置
        $captcha['reset'] = $this->request->param('reset',true);
        // 过期时间
        $captcha['expire'] = $this->request->param('expire',60*5);
        // 批量设置参数
        Config::set($captcha, 'captcha');
        return ThinkCaptcha::create();
    }
}
