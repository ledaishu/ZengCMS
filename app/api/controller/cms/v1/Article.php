<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 文章接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use think\facade\Db;
use app\api\controller\BaseController;

class Article  extends BaseController 
{
    /**
     * [index 文章列表]
     * url /article/index
     * HTTP POST
     * @return void
     */
    public function index()
    {
        $size = (int)input('size');
        $page = (int)input('page');
        
        $size = $size?$size:10;
        $page = $page?$page:1;

        // 栏目id
        $category_id = trim(input('category_id') ? input('category_id') : 4);
        $map = [];
        // 获取所有子栏目ID包含自身-相同模型
        $typeids = getAllChildcateIds_same_model($category_id);
        if (!empty($typeids)) {
            $typeids = explode(',', $typeids);
            $map[] = ['category_id', 'in', $typeids];
        }
        $list = Db::name('document')->field('id,title,icon,tags,description,create_time')
        ->where($map)
        ->order(['sort' => 'desc', 'id' => 'desc'])
        ->paginate([
            'list_rows'=> $size,
            'var_page' => 'page',
            'page'=>$page,
        ]);
        foreach($list as $k=>$v){
            if($v['icon']){
                $v['icon'] = 'http://admin.yanjianapp.com'.get_file_path($v['icon']);
            }else{
                $v['icon'] = 'http://admin.yanjianapp.com/static/uploads/default/defaultpic.jpg';
            }
            $v['tags'] = explode(',',$v['tags']);
            $v['description'] = $this->StrToArray($v['description']);
            $v['create_time'] = date('Y年m月d日',$v['create_time']);
            $list[$k] = $v;
        }
        return $this->returnData([
            'code'=>0,
            'message'=>'success',
            'list' => $list,
        ]);
    }
    /**
     * [detail 文章内容]
     * @url /article/detail
     * @HTTP POST
     * @return void
     */
    public function detail()
    {
        $id = input('id');
        $id = (int)$id;
        $data = Db::name('document')
        ->field('title,tags,create_time,content')
        ->where('id',$id)->find();
        if($data){
            $data['tags'] = explode(',',$data['tags']);
            $data['create_time'] = date('Y年m月d日 '.$this->getStrTime($data['create_time']).'h:i',$data['create_time']);
        }
        $pre_page = [];
        $next_page = [];
        $pre_id = $id - 1;
        if($pre_id > 0){
            $pre_page = Db::name('document')->field('id,title')->where('id',$pre_id)->find();
        }
        $next_id = $id + 1;
        $next_page = Db::name('document')->field('id,title')->where('id',$next_id)->find();
        return $this->returnData([
            'code'=>0,
            'message'=>'success',
            'data'=>$data,
            'pre_page'=>$pre_page,
            'next_page'=>$next_page,
        ]);
    }
    private function getStrTime($time)
    {
        $no=date("H",$time);
        if ($no>0 && $no<=6){
            return "凌晨 ";
        }
        if ($no>6 && $no<12){
            return "上午 ";
        }
        if ($no>=12 && $no<=18){
            return "下午 ";
        }
        if ($no>18 && $no<=24){
            return "晚上 ";
        }
        return "";
    }
    /**
     * 字符串转数组
     * @param [type] $str
     * @return void
     */
    private function StrToArray($str)
    {
        $array = str_replace("\r"," ",$str);
        $array = str_replace("\n"," ",$array);
        $array = explode(" ",$array);
        return array_filter($array);
    }
}

