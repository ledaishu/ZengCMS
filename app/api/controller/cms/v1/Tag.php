<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 标签接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use think\facade\Db;
// 自定义基础控制器
use app\api\controller\BaseController;

class Tag  extends BaseController 
{
    /**
     * [get_hot_tag 热门标签]
     * url /get_hot_tag
     * HTTP POST
     * @return void
     */
    public function get_hot_tag()
    {
        $list = Db::name('tag')->field('tag_name')
        ->order(['click_num'=>'desc','sort'=>'desc'])
        ->limit(20)
        ->select()
        ->toArray();
        return $this->return_data($list);
    }
    /**
     * tag文章列表
     * @url /tag/article
     * @HTTP POST
     * @return void
     */
    public function article()
    {
        $tag = input('tag','');
        $size = (int)input('size');
        $page = (int)input('page');
        
        $size = $size?$size:10;
        $page = $page?$page:1;
        
        $list = Db::name('document')->field('id,title,icon,tags,description,create_time')
        ->where("find_in_set('$tag',tags)")
        // ->whereFindInSet()
        ->order(['sort' => 'desc', 'id' => 'desc'])
        ->paginate([
            'list_rows'=> $size,
            'var_page' => 'page',
            'page'=>$page,
        ]);
        foreach($list as $k=>$v){
            if($v['icon']){
                $v['icon'] = 'http://admin.yanjianapp.com'.get_file_path($v['icon']);
            }else{
                $v['icon'] = 'http://admin.yanjianapp.com/static/uploads/default/defaultpic.jpg';
            }
            $v['tags'] = explode(',',$v['tags']);
            $v['description'] = $this->StrToArray($v['description']);
            $v['create_time'] = date('Y年m月d日',$v['create_time']);
            $list[$k] = $v;
        }
        return $this->returnData([
            'code'=>0,
            'message'=>'success',
            'list' => $list,
        ]);
    }
    /**
     * 字符串转数组
     * @param [type] $str
     * @return void
     */
    private function StrToArray($str)
    {
        $array = str_replace("\r"," ",$str);
        $array = str_replace("\n"," ",$array);
        $array = explode(" ",$array);
        return array_filter($array);
    }
}

