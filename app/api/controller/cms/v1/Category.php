<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 栏目接口
// +----------------------------------------------------------------------
namespace app\api\controller\v1;

use tree\Tree;
use think\facade\Db;
use app\api\controller\BaseController;

class Category  extends BaseController 
{
    /**
     * [Description 首页底部资讯中心]
     * @url /category/information_center
     * @HTTP POST
     * @DateTime 2020-04-30 11:58:12
     * @return void
     */
    public function information_center()
    {
        $category = Db::name('arctype')
        ->field('id,typename,name,icon,model_id')
        ->where('id','in','8,9,10')->select()->toArray();
        foreach($category as $k=>$v){
            $url = get_file_path($v['icon']);
            if(strpos($url,'http') === false){
                $url = 'http://admin.yanjianapp.com'.$url;
            }
            $v['icon'] = $url;
            $document = Db::name('document')->field('id,title')->where('category_id',$v['id'])->select()->toArray();
            if($document){
                foreach($document as $k2=>$v2){
                    $v2['title'] = cut_str($v2['title'],13);
                    $v2['model_id'] = $v['model_id'];
                    $document[$k2] = $v2;
                }
                $v['list'] = $document;
            }else{
                $v['list'] = [];
            }  
            $category[$k] = $v;
        }
        return $this->return_data($category);
    }
    /**
     * [Description 资讯中心右侧分类]
     * url /category/get_cate
     * HTTP POST
     * @return void
     */
    public function get_cate()
    {
        // 获取所有子栏目ID包含自身-相同模型
        $typeids = getAllChildcateIds_same_model(4);
        if (!empty($typeids)) {
            $typeids = explode(',', $typeids);
            $map[] = ['id', 'in', $typeids];
        }
        $tree = new Tree();
        $data = Db::name('arctype')->field('id,typename,name,pid')->where('status',1)->order(['sort'=>'desc','id'=>'asc'])->where($map)->select()->toArray();
        $list = $tree->ChildrenTree($data, 4);
        return $this->return_data($list);
    }
    /**
     * [detail 分类内容]
     * url /category/detail
     * HTTP POST
     * @return void
     */
    public function detail()
    {
        $id = input('id/d');
        $data = Db::name('arctype')->field('typename,content')->where('id',$id)->find();
        return $this->return_data($data);
    }
}

