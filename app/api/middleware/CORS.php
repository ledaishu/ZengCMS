<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | 解决CORS跨域问题
// +----------------------------------------------------------------------
namespace app\api\middleware;

class CORS
{
    public function handle($request, \Closure $next)
    {
        // 添加中间件执行代码
        header('Access-Control-Allow-Origin:*');
        header('Access-Control-Allow-Methods:*');
        header('Access-Control-Allow-Headers:authorization,Authorization,DNT,X-CustomHeader,Keep-Alive,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type');
        header('Access-Control-Allow-Credentials:false');
        // 判断是否OPTIONS请求
        if (request()->isOptions()) {
            exit();
        }
        return $next($request);
    }
}
