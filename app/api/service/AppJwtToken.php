<?php
// +----------------------------------------------------------------------
// | ZengCMS [ 火火 ]
// +----------------------------------------------------------------------
// | Copyright (c) 2018 http://zengcms.com All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 火火 <zengcms@qq.com>
// +----------------------------------------------------------------------

// +----------------------------------------------------------------------
// | AppJwtToken服务类
// +----------------------------------------------------------------------
namespace app\api\service;

use app\api\model\ThirdApp;
use app\api\lib\exception\MissException;

class AppJwtToken extends JwtToken
{
    /**
     * 第三方应用获取令牌
     * $ac和$se在数据库最好加密，明文不好不安全
     * @param  [type] $ac [description]
     * @param  [type] $se [description]
     * @return [type]     [description]
     */
    public function get($ac, $se)
    {
        $app = ThirdApp::check($ac, $se);
        if(!$app){
            throw new MissException([
                'message'=>'授权失败！'
            ]);
        }else{
            $uid = $app->id;
            $values = [
                'uid' => $uid,
                'ip'  => get_reluser_ip(),
                'time'=> time()
            ];
            // 获取令牌token
            $token = self::generateToken($values);
            return $token;
        }
    }
}